#include "rpmap.h"

/**
@class RPMap
@brief Interface for maps for the AI module

RPMap provides a generic interface for varying kinds of
maps for the AI module. Algorithms provided by the AI module
are programmed against this interface to be able to support several map
formats.

@ingroup rpai
*/

/**
@fn RPMap::childNodes(const RPMapNode &node ) const
@brief Child nodes for the given node

Returns a list with all child nodes that can be reached from the given
@p node. This takes into account the context assigned to the node object.
*/
