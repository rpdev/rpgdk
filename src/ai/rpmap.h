#ifndef RPMAP_H
#define RPMAP_H

#include <rpmapnode.h>

class RPMap
{
public:

  virtual ~RPMap() {}

  virtual RPMapNodeList childNodes( const RPMapNode &node ) const = 0;

};

#endif // RPMAP_H
