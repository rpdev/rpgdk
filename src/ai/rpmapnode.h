#ifndef RPMAPNODE_H
#define RPMAPNODE_H

#include <QList>

class RPMapNodeContext
{
};

class RPMapNode
{

public:

    RPMapNode( int id = -1, RPMapNodeContext *context = 0 );
    RPMapNode( const RPMapNode &other );
    virtual ~RPMapNode();

    int id() const;
    RPMapNodeContext* context() const;

    bool operator ==( const RPMapNode &other );
    bool operator !=( const RPMapNode &other );
    const RPMapNode& operator = ( const RPMapNode &that );

private:

    class RPMapNodePrivate;
    RPMapNodePrivate *d;

};

typedef QList< RPMapNode > RPMapNodeList;


#endif // RPMAPNODE_H
