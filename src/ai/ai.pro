TEMPLATE = lib
TARGET = RPAI
QT += core
SOURCES = \
    rpmapnode.cpp \
    rpmap.cpp
PUBLICHEADERS =
HEADERS = $${PUBLICHEADERS} \
    rpmapnode.h \
    rpmap.h
INCLUDEPATH += . ..
VERSION = 0.0.0
DEFINES += RPAI_LIB



# Install the RPAI library and headers
target.path = $${INSTALLPREFIX}/lib$${LIBSUFFIX}

RPAIInstallHeaders.path = $${INSTALLPREFIX}/include/rpgdk/ai
RPAIInstallHeaders.files = $${PUBLICHEADERS}

RPAIFancyHeaders.path = $${INSTALLPREFIX}/include/rpgdk
RPAIFancyHeaders.files =

INSTALLS += target RPAIInstallHeaders RPAIFancyHeaders
