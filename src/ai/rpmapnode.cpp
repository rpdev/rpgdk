#include "rpmapnode.h"

/**
  @class RPMapNodeContext
  @brief Context of a node in a map

  The RPMapNodeContext class is used as base for classes providing context
  information if a node in a map.

  @ingroup rpai
  */

/**
  @typedef RPMapNodeList
  @brief A list of nodes in a map

  The RPMapNodeList class holds a list of nodes of a map.

  @ingroup rpai
  */

/**
  @private
  */
class RPMapNode::RPMapNodePrivate
{
public:

  RPMapNodeContext *context;
  int               id;

  RPMapNodePrivate( RPMapNode *node ) :
    context( 0 ),
    id( -1 ),
    q( node )
  {

  }

  virtual ~RPMapNodePrivate()
  {
  }

private:

  RPMapNode *q;

};

/**
  @class RPMapNode
  @brief Represents a single node in a map

  The RPMapNode class is used to represent a single node in a map.
  The algorithms provided by the AI module are programmed against this
  class to be able to serve varying kinds of maps.
  */

/**
  @brief Creates a new node

  Creates a new map node using the given @p id and @p context.

  @note Though not forbidden explicitly, it is strongy recommended to not
  use this directly. Usually, the map node is created by a map implementation
  which uses to store information in the node object which is later used for
  identification of nodes.
  */
RPMapNode::RPMapNode( int id, RPMapNodeContext *context ) :
  d( new RPMapNodePrivate( this ) )
{
  d->id = id;
  d->context = context;
}

/**
  @brief Copy constructor

  Creates a map node from the @o other node.
  */
RPMapNode::RPMapNode( const RPMapNode &other ) :
  d( new RPMapNodePrivate( this ) )
{
  d->id = other.d->id;
  d->context = other.d->context;
}

/**
  @brief Destructor
  */
RPMapNode::~RPMapNode()
{
  delete d;
}

/**
  @brief The ID of the node

  A numeric ID used to identify the node.

  @note Two nodes are considered to refer to the same position
  on a map, when they have the same ID. Thus, this ID usually is
  calculated from the coordinates of a node inside the map. The concrete
  equation for getting the ID from the coordinates (or vice verse) however
  depends on the concrete map implementation.
  */
int RPMapNode::id() const
{
  return d->id;
}

/**
  @brief Context information for the node.

  Returns a pointer to a context object associated with the node.
  */
RPMapNodeContext* RPMapNode::context() const
{
  return d->context;
}

/**
  @brief Test whether two nodes are equal.

  Returns true if the ID if this node equals the id of the @p other node.
  */
bool RPMapNode::operator ==( const RPMapNode &other )
{
  return d->id == other.d->id;
}

/**
  @brief Test whether two nodes are unequal

  Returns true if the ID of this node is not equal to the id of the @p other
  node.
  */
bool RPMapNode::operator !=( const RPMapNode &other )
{
  return !( *this == other );
}

/**
  @brief Copies a node

  Copies the values of the node @p that into this node.
  */
const RPMapNode& RPMapNode::operator =( const RPMapNode &that )
{
  d->id = that.d->id;
  d->context = that.d->context;
  return *this;
}
