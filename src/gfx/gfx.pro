TEMPLATE = lib
TARGET = RPGfx
QT += core opengl
SOURCES = \
    rptexturemanager.cpp \
    rptexture.cpp \
    rptextureinternal.cpp \
    rpview.cpp \
    rporthoview.cpp \
    rpobject.cpp \
    rpdrawable.cpp \
    rpcamera.cpp \
    rplookatcamera.cpp
PUBLICHEADERS = \
    rptexturemanager.h \
    rptexture.h \
    RPGfxConfig.h \
    rpview.h \
    rporthoview.h \
    rpobject.h \
    rpdrawable.h \
    rpcamera.h \
    rplookatcamera.h
HEADERS = $${PUBLICHEADERS} \
    rptextureinternal.h
INCLUDEPATH += . ..
VERSION = 0.0.0
DEFINES += RPGFX_LIB



# Install the RPGfx library and headers
target.path = $${INSTALLPREFIX}/lib$${LIBSUFFIX}

RPGfxInstallHeaders.path = $${INSTALLPREFIX}/include/rpgdk/gfx
RPGfxInstallHeaders.files = $${PUBLICHEADERS}

RPGfxFancyHeaders.path = $${INSTALLPREFIX}/include/rpgdk
RPGfxFancyHeaders.files = inc/RPCamera \
                RPDrawable \
                RPGfx \
                RPLookAtCamera \
                RPObject \
                RPOrthoView \
                RPTexture \
                RPTextureManager \
                RPView

INSTALLS += target RPGfxInstallHeaders RPGfxFancyHeaders
