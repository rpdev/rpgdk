/*
    RP/GDK
    Copyright (C) 2011  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "rporthoview.h"

#include <QtOpenGL>

/**
  @internal
  */
class RPOrthoView::RPOrthoViewPrivate
{

public:

    double left;
    double right;
    double top;
    double bottom;
    double near;
    double far;

    RPOrthoViewPrivate() :
        left( 0.0 ),
        right( 100.0 ),
        top( 100.0 ),
        bottom( 0.0 ),
        near( 0.1 ),
        far( 100.0 )
    {
    }

    virtual ~RPOrthoViewPrivate()
    {
    }

private:

};

/**
  @class RPOrthoView
  @brief Provides orthogonal view.

  In orthogonal views, objects are rastered in a "flat" way, i.e. the distance between
  the camera and an object does not have any influence on how the object is rendered
  (apart from the fact, that the distance might cause an object to be clipped).

  Orthogonal views are described by a left, right, top and bottom coordinate. Note, that
  by default bottom is smaller than top. One can however set top to be less than
  bottom (which is more common in windowing systems as the WinAPI or X).

  @ingroup RPGfx
  */

/**
  @brief Constructor.
  */
RPOrthoView::RPOrthoView( QObject *parent ) :
    RPView( parent ),
    d( new RPOrthoViewPrivate() )
{
}

/**
  @brief Destructor.
  */
RPOrthoView::~RPOrthoView()
{
    delete d;
}

// Implementation of RPView::activate
void RPOrthoView::activate()
{
    glMatrixMode( GL_PROJECTION );
    glOrtho( d->left, d->right, d->bottom, d->top, d->near, d->far );
    glMatrixMode( GL_PROJECTION );
}

/**
  @brief Left coordinate of the scene.
  */
double RPOrthoView::left() const
{
    return d->left;
}

/**
  @brief Right coordinate of the scene.
  */
double RPOrthoView::right() const
{
    return d->right;
}

/**
  @brief Top coordinate of the scene.
  */
double RPOrthoView::top() const
{
    return d->top;
}

/**
  @brief Bottom coordinate of the scene.
  */
double RPOrthoView::bottom() const
{
    return d->bottom;
}

/**
  @brief Near clipping plane.
  */
double RPOrthoView::near() const
{
    return d->near;
}

/**
  @brief Far clipping plane.
  */
double RPOrthoView::far() const
{
    return d->far;
}

/**
  @brief Sets the left coordinate.
  */
void RPOrthoView::setLeft( double left )
{
    d->left = left;
}

/**
  @brief Sets the right coordinate.
  */
void RPOrthoView::setRight( double right )
{
    d->right = right;
}

/**
  @brief Sets the top coordinate.
  */
void RPOrthoView::setTop( double top )
{
    d->top = top;
}

/**
  @brief Sets the bottom coordinate.
  */
void RPOrthoView::setBottom( double bottom )
{
    d->bottom = bottom;
}

/**
  @brief Sets the near clipping plane.
  */
void RPOrthoView::setNear( double near )
{
    d->near = near;
}

/**
  @brief Sets the far clipping plane.
  */
void RPOrthoView::setFar( double far )
{
    d->far = far;
}
