/*
    RP/GDK
    Copyright (C) 2011  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "rptexture.h"

#include "rptexturemanager.h"
#include "rptextureinternal.h"

/**
  @internal
  */
class RPTexture::RPTexturePrivate
{

public:

    RPTextureManager *manager;
    RPTextureInternal *internal;

    RPTexturePrivate()
    {
    }

    virtual ~RPTexturePrivate()
    {
    }

};

/**
  @class RPTexture
  @brief A texture object.

  Texture ojects encapsulate OpenGL textures. Additionally, they
  provide internal caching mechanisms, so image files are only uploaded
  when they really need to.

  @ingroup RPGfx
  */

/**
  @brief Deletes a texture object.
  */
RPTexture::~RPTexture()
{
    d->internal->unref();
    delete d;
}

/**
  @brief Make the texture active.
  */
void RPTexture::bind() const
{
    glBindTexture( d->internal->target(), d->internal->texture() );
}

RPTexture::RPTexture( RPTextureManager *manager,
                     RPTextureInternal *internal,
                     QObject *parent ) :
    QObject(parent),
    d( new RPTexturePrivate() )
{
    d->manager = manager;
    d->internal = internal;
    internal->ref();
}
