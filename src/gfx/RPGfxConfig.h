/*
    RP/GDK
    Copyright (C) 2011  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RPGFXCONFIG_H
#define RPGFXCONFIG_H

#include <QtGlobal>

#ifdef RPGFX_LIB
#define RPGFX_EXPORT Q_DECL_EXPORT
#else
#define RPGFX_EXPORT Q_DECL_IMPORT
#endif

#endif // RPGFXCONFIG_H
