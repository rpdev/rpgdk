/*
    RP/GDK
    Copyright (C) 2011  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RPOBJECT_H
#define RPOBJECT_H

#include <QObject>
#include <QVector3D>


class RPObject : public QObject
{

    Q_OBJECT

    Q_PROPERTY( QVector3D position READ position WRITE setPosition )

public:

    explicit RPObject(QObject *parent = 0);    
    virtual ~RPObject();

    const QVector3D& position() const;

signals:

    void positionChanged();

public slots:

    void setPosition( const QVector3D &position );

private:

    class RPObjectPrivate;
    RPObjectPrivate *d;

};

#endif // RPOBJECT_H
