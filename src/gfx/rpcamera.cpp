/*
    RP/GDK
    Copyright (C) 2011  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "rpcamera.h"

#include <QtOpenGL>

/**
@class RPCamera
@brief Base class for camera objects.


 A camera defines the actual position of the viewer in the scene plus
 additional features such as e.g. the direction into which he views.
 @ingroup RPGfx
 */

/**
@fn RPCamera::activate
@brief Activate this camera.
*/

/**
@brief Constructor.
*/
RPCamera::RPCamera( QObject *parent ) :
    RPObject( parent )
{
}

/**
@brief Destructor.
*/
RPCamera::~RPCamera()
{
}

/**
@brief Saves the current camera position.
*/
void RPCamera::push() const
{
    glMatrixMode( GL_PROJECTION );
    glPushMatrix();
    glMatrixMode( GL_MODELVIEW );
}

/**
@brief Restores a saved camera position.
*/
void RPCamera::pop() const
{
    glMatrixMode( GL_PROJECTION );
    glPopMatrix();
    glMatrixMode( GL_MODELVIEW );
}
