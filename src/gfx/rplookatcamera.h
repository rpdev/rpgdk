/*
    RP/GDK
    Copyright (C) 2011  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RPLOOKATCAMERA_H
#define RPLOOKATCAMERA_H

#include "rpcamera.h"


class RPLookAtCamera : public RPCamera
{

    Q_OBJECT

    Q_PROPERTY( QVector3D target READ target WRITE setTarget )
    Q_PROPERTY( QVector3D up READ up WRITE setUp )

public:

    explicit RPLookAtCamera( QObject *parent = 0 );
    virtual ~RPLookAtCamera();

    const QVector3D& target() const;
    const QVector3D& up() const;

    // Implemented from RPCamera
    virtual void activate();

public slots:

    void setTarget( const QVector3D &target );
    void setUp( const QVector3D &up );

private:

    class RPLookAtCameraPrivate;
    RPLookAtCameraPrivate *d;
};

#endif // RPLOOKATCAMERA_H
