/*
    RP/GDK
    Copyright (C) 2011  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "rpobject.h"

/**
  @internal
  */
class RPObject::RPObjectPrivate
{

public:

    QVector3D position;

    RPObjectPrivate() :
        position( 0.0, 0.0, 0.0 )
    {
    }

    ~RPObjectPrivate()
    {
    }

private:

};

/**
  @class RPObject
  @brief Base class for all objects in 2D and 3D.

  RPObject is a base for all objects in both 2D and 3D space.
  This includes objects that can be rendered but also ones, that are just
  functional (e.g. RPCamera).

  @ingroup RPGfx
  */

/**
  @fn RPObject::positionChanged
  @brief The object has been moved.
  */

/**
  @brief Constructor.
  */
RPObject::RPObject(QObject *parent) :
    QObject(parent),
    d( new RPObjectPrivate() )
{
}

/**
  @brief Destructor.
  */
RPObject::~RPObject()
{
    delete d;
}

/**
  @brief The current position of the object.
  */
const QVector3D& RPObject::position() const
{
    return d->position;
}

/**
  @brief Move the object.

  This sets the current position of the object.
  This will cause the positionChanged() signal to be emitted
  immediately after the new position has been applied.
  */
void RPObject::setPosition( const QVector3D &position )
{
    d->position = position;
    emit positionChanged();
}
