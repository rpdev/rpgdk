/*
    RP/GDK
    Copyright (C) 2011  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RPTEXTUREMANAGER_H
#define RPTEXTUREMANAGER_H

#include "RPGfxConfig.h"

#include "RPTexture"

#include <QGLContext>
#include <QImage>
#include <QObject>
#include <QPixmap>


class RPGFX_EXPORT RPTextureManager : public QObject
{

    Q_OBJECT

public:

    explicit RPTextureManager( QGLContext *context, QObject *parent = 0 );
    virtual ~RPTextureManager();

    RPTexture* createTexture( const QString &filename,
                             GLenum target = GL_TEXTURE_2D,
                             GLint format = GL_RGBA,
                             QGLContext::BindOptions opts = QGLContext::DefaultBindOption,
                             QObject *parent = 0 );
    RPTexture* createTexture( QPixmap pixmap,
                             GLenum target = GL_TEXTURE_2D,
                             GLint format = GL_RGBA,
                             QGLContext::BindOptions opts = QGLContext::DefaultBindOption,
                             bool keepPixmap = false,
                             QObject *parent = 0 );
    RPTexture* createTexture( QImage image,
                             GLenum target = GL_TEXTURE_2D,
                             GLint format = GL_RGBA,
                             QGLContext::BindOptions opts = QGLContext::DefaultBindOption,
                             bool keepImage = false,
                             QObject *parent = 0 );

signals:

public slots:

private:

    class RPTextureManagerPrivate;
    RPTextureManagerPrivate *d;

    RPTextureManagerPrivate* d_ptr() const;
    const RPTextureManagerPrivate* d_constptr() const;

private slots:

    void unloadTexture();

};

#endif // RPTEXTUREMANAGER_H
