/*
    RP/GDK
    Copyright (C) 2011  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RPTEXTUREINTERNAL_H
#define RPTEXTUREINTERNAL_H

#include <QGLContext>
#include <QImage>
#include <QObject>
#include <QPixmap>

class RPTextureInternal : public QObject
{

    Q_OBJECT

public:

    explicit RPTextureInternal( QGLContext *ctx, QObject *parent = 0 );
    virtual ~RPTextureInternal();

    void createTexture( bool dry,
                        const QString &filename,
                        GLenum target = GL_TEXTURE_2D,
                        GLint format = GL_RGBA,
                        QGLContext::BindOptions opts = QGLContext::DefaultBindOption );
    void createTexture( bool dry,
                        QPixmap pixmap,
                        GLenum target = GL_TEXTURE_2D,
                        GLint format = GL_RGBA,
                        QGLContext::BindOptions opts = QGLContext::DefaultBindOption,
                        bool keepPixmap = false );
    void createTexture( bool dry,
                        QImage image,
                        GLenum target = GL_TEXTURE_2D,
                        GLint format = GL_RGBA,
                        QGLContext::BindOptions opts = QGLContext::DefaultBindOption,
                        bool keepImage = false );

    inline const QString& filename() const;
    inline QImage image() const;
    inline QPixmap pixmap() const;
    inline GLenum target() const;
    inline GLint format() const;
    inline QGLContext::BindOptions bindOptions() const;
    inline GLuint texture() const;

    void ref();
    void unref();
    bool isUnreferenced() const;

    uint hashValue() const;

    bool operator == ( const RPTextureInternal &other ) const;

signals:

    void canBeDeleted();

public slots:

private:

    QString                     m_filename;
    QImage                      m_image;
    QPixmap                     m_pixmap;
    GLenum                      m_target;
    GLint                       m_format;
    QGLContext::BindOptions     m_bindOptions;
    QGLContext                 *m_context;

    uint                        m_ctime;

    GLuint                      m_texture;
    uint                        m_referenceCount;

    static uint getCtime();

};

inline const QString& RPTextureInternal::filename() const
{
    return m_filename;
}

inline QImage RPTextureInternal::image() const
{
    return m_image;
}

inline QPixmap RPTextureInternal::pixmap() const
{
    return m_pixmap;
}

inline GLenum RPTextureInternal::target() const
{
    return m_target;
}

inline GLint RPTextureInternal::format() const
{
    return m_format;
}

inline QGLContext::BindOptions RPTextureInternal::bindOptions() const
{
    return m_bindOptions;
}

inline GLuint RPTextureInternal::texture() const
{
    return m_texture;
}

inline bool RPTextureInternal::isUnreferenced() const
{
    return m_referenceCount == 0;
}

uint qHash( const RPTextureInternal *ti );

#endif // RPTEXTUREINTERNAL_H
