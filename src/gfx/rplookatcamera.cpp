/*
    RP/GDK
    Copyright (C) 2011  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "rplookatcamera.h"

#include <QtOpenGL>

/**
  @brief Private class members for RPLookAtCamera.

  @internal
  */
class RPLookAtCamera::RPLookAtCameraPrivate
{

public:

    QVector3D target;
    QVector3D up;

    RPLookAtCameraPrivate() :
        target( 0.0, 0.0, -10.0 ),
        up( 0.0, 1.0, 0.0 )
    {
    }

    virtual ~RPLookAtCameraPrivate()
    {

    }

private:

};


/**
  @class RPLookAtCamera
  @brief Camera, that looks at a given target.

  The look-at camera can be used to focus the view into a given object or position
  in space. Basically, the camera uses it's own position, the target position and an
  up vector to calculate a projection matrix.

  @ingroup RPGfx
  */

/**
  @brief Creates a new look-at camera.
  */
RPLookAtCamera::RPLookAtCamera( QObject *parent ) :
    RPCamera( parent ),
    d( new RPLookAtCameraPrivate() )
{
}

/**
  @brief Deletes the camera.
  */
RPLookAtCamera::~RPLookAtCamera()
{
    delete d;
}

/**
  @brief The target the camera is focused on.
  */
const QVector3D& RPLookAtCamera::target() const
{
    return d->target;
}

/**
  @brief The up vector used to calculate the view.
  */
const QVector3D& RPLookAtCamera::up() const
{
    return d->up;
}

// Reimplemented from RPCamera
void RPLookAtCamera::activate()
{
    glMatrixMode( GL_PROJECTION );
    glLoadIdentity();
    gluLookAt( position().x(), position().y(), position().z(),
              d->target.x(), d->target.y(), d->target.z(),
              d->up.y(), d->target.y(), d->target.z() );
    glMatrixMode( GL_MODELVIEW );
}


/**
  @brief Sets the target, the camera is focussing on.
  */
void RPLookAtCamera::setTarget( const QVector3D &target )
{
    d->target = target;
}

/**
  @brief Sets the up vector.
  */
void RPLookAtCamera::setUp( const QVector3D &up )
{
    d->up = up;
}
