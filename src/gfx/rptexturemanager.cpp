/*
    RP/GDK
    Copyright (C) 2011  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "rptexturemanager.h"

#include "rptextureinternal.h"

#include <QDebug>
#include <QHash>

/**
  @internal
  */
class RPTextureManager::RPTextureManagerPrivate
{
public:

    QGLContext *context;
    QHash< RPTextureInternal*, RPTextureInternal* > textures;

    RPTextureManagerPrivate()
    {
    }

    virtual ~RPTextureManagerPrivate()
    {
    }

};

/**
  @class RPTextureManager
  @brief Manages texture object.

  The texture manager is used to load and manage textures.
  It needs an OpenGL context, so it can upload the texture data and
  create the necessary texture in video RAM.

  The manager provides some caching behavior, so if one tries to load a texture from
  the same file twice, it is indeed only loaded once and the second copy is a cheap reuse
  of the first one's data.

  The caching works only when the texture manager is dealing with files. However,
  it is also able to create textures from image data in memory by accepting QImage and
  QPixmap objects. In this case, caching will be turned of unless the texture is
  created keeping the image or pixmap in memory.

  @ingroup RPGfx
  */

/**
  @brief Creates a new texture manager.

  @param context The OpenGL context object to which the texture manager should belong.
  @param parent The parent object which owns the texture manager.
  */
RPTextureManager::RPTextureManager( QGLContext *context, QObject *parent ) :
    QObject( parent ),
    d( new RPTextureManagerPrivate() )
{
    d->context = context;
}

/**
  @brief Deletes a texture manager.
  */
RPTextureManager::~RPTextureManager()
{
    delete d;
}

/**
  @brief Create a new texture object.

  This creates a new texture object, loading it from file on disk or in a resource.

  @param filename The file from which to load the image.
  @param target The OpenGL target to which the texture will be bound.
  @param format The format to use for the texture.
  @param opts Bind options for the texture.
  @param parent The object which will own the texture object.

  @note This is the preferred method for loading textures.
  */
RPTexture* RPTextureManager::createTexture( const QString &filename,
                                           GLenum target,
                                           GLint format,
                                           QGLContext::BindOptions opts,
                                           QObject *parent )
{
    RPTextureInternal *ti = new RPTextureInternal( d_constptr()->context, this );
    ti->createTexture( true, filename, target, format, opts );
    RPTextureInternal *data = 0;
    data = d_constptr()->textures.value( ti, 0 );
    if ( !( data ) )
    {
        data = ti;
        data->createTexture( false, filename, target, format, opts );
        connect( data, SIGNAL(canBeDeleted()), this, SLOT(unloadTexture()) );
        d_ptr()->textures.insert( data, data );
    }
    RPTexture *texture = new RPTexture( this, data, parent );
    return texture;
}

/**
  @brief Create a new texture object.

  This creates a new texture object from the given pixmap.

  @param pixmap The pixmap to upload into video RAM.
  @param target The OpenGL target to which the texture will be bound.
  @param format The format to use for the texture.
  @param opts Bind options for the texture.
  @param keepPixmap If set to true, the texture will keep a copy of the pixmap internally.
  @param parent The object which will own the texture object.
  */
RPTexture* RPTextureManager::createTexture( QPixmap pixmap,
                                           GLenum target,
                                           GLint format,
                                           QGLContext::BindOptions opts,
                                           bool keepPixmap,
                                           QObject *parent )
{
    RPTextureInternal *ti = new RPTextureInternal( d_constptr()->context, this );
    ti->createTexture( true, pixmap, target, format, opts, keepPixmap );
    RPTextureInternal *data = 0;
    data = d_constptr()->textures.value( ti, 0 );
    if ( !( data ) )
    {
        data = ti;
        data->createTexture( false, pixmap, target, format, opts, keepPixmap );
        connect( data, SIGNAL(canBeDeleted()), this, SLOT(unloadTexture()) );
        d_ptr()->textures.insert( data, data );
    }
    RPTexture *texture = new RPTexture( this, data, parent );
    return texture;
}

/**
  @brief Create a new texture object.

  This creates a new texture object from the given image.

  @param image The image to upload into video RAM.
  @param target The OpenGL target to which the texture will be bound.
  @param format The format to use for the texture.
  @param opts Bind options for the texture.
  @param keepImage If set to true, the texture will keep a copy of the image internally.
  @param parent The object which will own the texture object.
  */
RPTexture* RPTextureManager::createTexture( QImage image,
                                           GLenum target,
                                           GLint format,
                                           QGLContext::BindOptions opts,
                                           bool keepImage,
                                           QObject *parent )
{
    RPTextureInternal *ti = new RPTextureInternal( d_constptr()->context, this );
    ti->createTexture( true, image, target, format, opts, keepImage );
    RPTextureInternal *data = 0;
    data = d_constptr()->textures.value( ti, 0 );
    if ( !( data ) )
    {
        data = ti;
        data->createTexture( false, image, target, format, opts, keepImage );
        connect( data, SIGNAL(canBeDeleted()), this, SLOT(unloadTexture()) );
        d_ptr()->textures.insert( data, data );
    }
    RPTexture *texture = new RPTexture( this, data, parent );
    return texture;
}

RPTextureManager::RPTextureManagerPrivate* RPTextureManager::d_ptr() const
{
    return d;
}

const RPTextureManager::RPTextureManagerPrivate* RPTextureManager::d_constptr() const
{
    return d;
}

void RPTextureManager::unloadTexture()
{
    RPTextureInternal *ti = qobject_cast< RPTextureInternal* >( sender() );
    if ( ti )
    {
        if ( d_ptr()->textures.contains( ti ) )
        {
            d_ptr()->textures.remove( ti );
            delete ti;
        } else
        {
            qWarning() << "Received RPTextureInternal in unloadTexture which is not in cache!";
        }
    } else
    {
        qWarning() << "Something triggered RPTextureManager::unloadTexture() which is no texture internal!";
    }
}
