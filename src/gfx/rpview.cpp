/*
    RP/GDK
    Copyright (C) 2011  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "rpview.h"

#include <QtOpenGL>

/**
  @class RPView
  @brief Base class for views.

  A view determines, how a scene generally appears to the user, i.e. whether
  orthogonal or perspective projection should be used.

  A RPView can be attached to a RPScene, which will then automatically activate
  its view when it draws.

  @ingroup RPGfx
  */

/**
  @fn RPView::activate
  @brief Activate this view.

  This will activate the view, i.e. set the projection mode that is described by it
  in the current scene. The push() and pop() methods can be used to preserve the
  mode previously set.
  */

/**
  @brief Constructor.
  */
RPView::RPView(QObject *parent) :
    QObject(parent)
{
}

/**
  @brief Destructor.
  */
RPView::~RPView()
{
}

/**
  @brief Save current view.

  This will save the current view. A call to pop() will then reactivate the view.
  When a view is attached to a scene, this will automatically be called.
  */
void RPView::push() const
{
    glMatrixMode( GL_PROJECTION );
    glPushMatrix();
    glMatrixMode( GL_MODELVIEW );
}

/**
  @brief Restore a saved view.

  This will restore a view saved via push().
  When the view is attached to a scene, this will automatically be called.
  */
void RPView::pop() const
{
    glMatrixMode( GL_PROJECTION );
    glPopMatrix();
    glMatrixMode( GL_MODELVIEW );
}
