/*
    RP/GDK
    Copyright (C) 2011  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RPORTHOVIEW_H
#define RPORTHOVIEW_H

#include "RPGfxConfig.h"

#include "RPView"

#include <QObject>


class RPGFX_EXPORT RPOrthoView : public RPView
{

    Q_OBJECT

    Q_PROPERTY( double left READ left WRITE setLeft )
    Q_PROPERTY( double right READ right WRITE setRight )
    Q_PROPERTY( double top READ top WRITE setTop )
    Q_PROPERTY( double bottom READ bottom WRITE setBottom )
    Q_PROPERTY( double near READ near WRITE setNear )
    Q_PROPERTY( double far READ far WRITE setFar )

public:

    explicit RPOrthoView( QObject *parent = 0 );
    virtual ~RPOrthoView();

    // Implementation of pure virtual methods from RPView:
    virtual void activate();

    double left() const;
    double right() const;
    double top() const;
    double bottom() const;
    double near() const;
    double far() const;

public slots:

    void setLeft( double left );
    void setRight( double right );
    void setTop( double top );
    void setBottom( double bottom );
    void setNear( double near );
    void setFar( double far );

private:

    class RPOrthoViewPrivate;
    RPOrthoViewPrivate *d;

};

#endif // RPORTHOVIEW_H
