/*
    RP/GDK
    Copyright (C) 2011  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "rptextureinternal.h"

#include <QHash>
#include <QTime>

#include <QDebug>

/**
  @class RPTextureInternal
  @internal
  */

/**
  @var RPTextureInternal::m_ctime
  @brief Time when the texture was created.

  This value is set in the createTexture methods. It defaults to zero
  when using the QString variant or letting image/pixmap data in memory; otherwise
  it is set to the current time. This is for hashing reasons.
  */

RPTextureInternal::RPTextureInternal( QGLContext *ctx, QObject *parent ) :
    QObject( parent ),
    m_filename( QString() ),
    m_image( QImage() ),
    m_pixmap( QPixmap() ),
    m_target( 0 ),
    m_format( 0 ),
    m_bindOptions( QGLContext::DefaultBindOption ),
    m_context( ctx ),
    m_ctime( 0 ),
    m_texture( 0 ),
    m_referenceCount( 0 )
{
}

RPTextureInternal::~RPTextureInternal()
{
    if ( m_texture != 0 )
    {
        m_context->makeCurrent();
        m_context->deleteTexture( m_texture );
    }
}

void RPTextureInternal::createTexture( bool dry,
                                      const QString &filename,
                                      GLenum target,
                                      GLint format,
                                      QGLContext::BindOptions opts )
{
    m_filename = filename;
    m_target = target;
    m_format = format;
    m_bindOptions = opts;
    if ( !( dry ) )
    {
        QImage image( filename );
        m_context->makeCurrent();
        m_texture = m_context->bindTexture( image, target, format, opts );
    }
}

void RPTextureInternal::createTexture( bool dry,
                                      QPixmap pixmap,
                                      GLenum target,
                                      GLint format,
                                      QGLContext::BindOptions opts,
                                      bool keepPixmap )
{
    if ( keepPixmap )
    {
        m_pixmap = pixmap;
    } else
    {
        m_ctime = RPTextureInternal::getCtime();
    }
    m_target = target;
    m_format = format;
    m_bindOptions = opts;
    if ( !( dry ) )
    {
        m_context->makeCurrent();
        m_texture = m_context->bindTexture( pixmap, target, format, opts );
    }
}

void RPTextureInternal::createTexture( bool dry,
                                      QImage image,
                                      GLenum target,
                                      GLint format,
                                      QGLContext::BindOptions opts,
                                      bool keepImage )
{
    if ( keepImage )
    {
        m_image = image;
    } else
    {
        m_ctime = RPTextureInternal::getCtime();
    }
    m_target = target;
    m_format = format;
    m_bindOptions = opts;
    if ( !( dry ) )
    {
        m_context->makeCurrent();
        m_texture = m_context->bindTexture( m_pixmap, target, format, opts );
    }
}

void RPTextureInternal::ref()
{
    m_referenceCount++;
}

void RPTextureInternal::unref()
{
    if ( m_referenceCount )
    {
        m_referenceCount--;
        if ( m_referenceCount == 0 )
        {
            emit canBeDeleted();
        }
    } else
    {
        qWarning() << "Trying to unref() a RPTextureInternal which is not referenced";
    }
}

uint RPTextureInternal::hashValue() const
{
    return qHash( m_filename ) ^
            qHash( m_ctime ) ^
            qHash( m_target ) ^
            qHash( m_format ) ^
            qHash( ( uint ) m_bindOptions );
}

bool RPTextureInternal::operator ==( const RPTextureInternal &other ) const
{
    if ( m_filename.isEmpty() && m_image.isNull() && m_pixmap.isNull() )
    {
        return false;
    }
    return ( m_filename == other.m_filename ) &&
            ( m_image == other.m_image ) &&
            ( m_pixmap.toImage() == other.m_pixmap.toImage() ) &&
            ( m_target == other.m_target ) &&
            ( m_format == other.m_format ) &&
            ( m_bindOptions == other.m_bindOptions );
}

uint RPTextureInternal::getCtime()
{
    QTime time = QTime::currentTime();
    return time.hour() * 3600000 +
            time.minute() * 60000 +
            time.second() * 1000 +
            time.msec();
}

uint qHash( RPTextureInternal *ti )
{
    return ti->hashValue();
}
