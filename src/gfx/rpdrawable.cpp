/*
    RP/GDK
    Copyright (C) 2011  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "rpdrawable.h"

/**
@class RPDrawable
@brief Base class for objects that can be drawn.

This class is a virtual base class for objects that can be drawn in 2D or
3D space. It inherits RPObject - thus it automatically carries some information
as e.g. the position in space with it.

Basically, this class provides a common interface, so drawable objects can be
drawn (and otherwise handled) automatically.

@ingroup RPGfx
*/

/**
@fn RPDrawable::draw
@brief Draws the object.
*/

/**
@brief Constructor.
*/
RPDrawable::RPDrawable( QObject *parent ) :
    RPObject( parent )
{
}

/**
@brief Destructor.
*/
RPDrawable::~RPDrawable()
{
}
